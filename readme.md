## Compressor 

[QT](https://en.wikipedia.org/wiki/Qt_(software)) interface for the compressor design feasibility using [NURBs](https://en.wikipedia.org/wiki/Non-uniform_rational_B-spline) and [Splines](https://en.wikipedia.org/wiki/Spline_(mathematics))

![interface.png](https://bitbucket.org/juanbaromance/compressor/downloads/compressor.png)